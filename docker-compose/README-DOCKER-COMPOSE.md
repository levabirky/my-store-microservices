# JHipster generated Docker-Compose configuration

## Usage

Launch all your infrastructure by running: `docker-compose up -d`.

## Configured Docker services

### Service registry and configuration server:

- [JHipster Registry](http://localhost:8761)

### Applications and dependencies:

- invoice (microservice application)
- invoice's mysql database
- notification (microservice application)
- notification's mysql database
- store (gateway application)
- store's mysql database

### Additional Services:

- [Prometheus server](http://localhost:9090)
- [Prometheus Alertmanager](http://localhost:9093)
- [Grafana](http://localhost:3000)


### Building and deploying everything to Docker locally

There are multiple ways in which we can use the Docker Compose files based on our needs.

In general, when we are developing the application, we can run the application with the Maven or Gradle command so that we can debug the application and reload the changes faster, as well as start the database and JHipster registry with Docker.

Otherwise, you can start the entire application from the app.yml file, which will kickstart the database, JHipster Registry, and then the application itself. To do that, open your Terminal or Command Prompt, go to each of the application folders, and run the following commands.

First, Dockerize the application by taking a production build of our application with the following command:

- ./gradlew bootJar -Pprod jibDockerBuild

Once done, we can start the app via the docker-compose command:

- docker-compose -f src/main/docker/app.yml up -d

Once you have done this for all three applications, we can check the running Docker containers with the following command:

- docker ps -a

### Generating Docker Compose files for microservices

There are many Docker Compose files and maintaining them is hard. Thankfully, JHipster has a docker-compose sub-generator bundled with it. The docker-compose sub-generator helps you organize all your application's Docker Compose files together. It creates a single Docker Compose file that refers to all the applications and their database, along with the registry and monitoring.

Let's go to the base folder, create a folder, and name it docker-compose:

- mkdir docker-compose && cd docker-compose

Once inside the docker-compose folder, we can run the following command:

- jhipster docker-compose 

...

- docker-compose up -d

### Docker Compose gives us the flexibility to scale our application with a single command with docker-compose:

- docker-compose up --scale <app-name>=<number of instance>

We can scale the instances using the following command:

- docker-compose up --scale invoice=2 invoice
