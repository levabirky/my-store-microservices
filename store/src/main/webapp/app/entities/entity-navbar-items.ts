export const EntityNavbarItems = [
  {
    name: 'ProductCategory',
    route: 'product-category',
    translationKey: 'global.menu.entities.productCategory',
  },
  {
    name: 'Customer',
    route: 'customer',
    translationKey: 'global.menu.entities.customer',
  },
  {
    name: 'Notification',
    route: 'notification',
    translationKey: 'global.menu.entities.notificationNotification',
  },
  {
    name: 'OrderItem',
    route: 'order-item',
    translationKey: 'global.menu.entities.orderItem',
  },
  {
    name: 'ProductOrder',
    route: 'product-order',
    translationKey: 'global.menu.entities.productOrder',
  },
  {
    name: 'Shipment',
    route: 'shipment',
    translationKey: 'global.menu.entities.invoiceShipment',
  },
  {
    name: 'Product',
    route: 'product',
    translationKey: 'global.menu.entities.product',
  },
  {
    name: 'Invoice',
    route: 'invoice',
    translationKey: 'global.menu.entities.invoiceInvoice',
  },
];
